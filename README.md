Для того, чтобы развернуть проект на Localhost, нужно:
1. С помощью Git получить файлы проекта командой: git clone https://gitlab.com/alex.grigorjev02/trpk_1
2. Установить зависимости программы командой npm install
3. Запустить сервер командой: node ./node_modules/pm2/bin/pm2 start , далее сервер запустится на адресе localhost:1917.
